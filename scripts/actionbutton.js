import Component from '../../../systems/custom-system-builder/module/sheets/components/Component.js';

export default class ActionButton extends Component {
    /**
     * Javascript to execute
     * @type {string|null}
     * @private
     */
    _actionButtonScript;

    /**
     * Button text
     * type {string|null}
     * @private
     */
    _actionButtonText;

    /**
     * Constructor
     * @param {Object} data Component data
     * @param {string} data.key Component key
     * @param {string|null} [data.tooltip] Component tooltip
     * @param {string} data.templateAddress Component address in template, i.e. component path from actor.system object
     * @param {string} [data.actionButtonScript] Javascript to execute on button click
     * @param {string} [data.actionButtonText] Button Text
     * @param {string|null} [data.cssClass=null] Additional CSS class to apply at render
     * @param {Number} [data.role=0] Component minimum role
     * @param {Number} [data.permission=0] Component minimum permission
     * @param {string|null} [data.visibilityFormula=null] Component visibility formula
     * @param {Container|null} [data.parent=null] Component's container
     */
    constructor({
        key,
        tooltip = null,
        templateAddress,
        actionButtonScript = null,
        actionButtonText = null,
        cssClass = null,
        role = 0,
        permission = 0,
        visibilityFormula = null,
        parent = null
    }) {
        super({
            key: key,
            tooltip: tooltip,
            templateAddress: templateAddress,
            cssClass: cssClass,
            role: role,
            permission: permission,
            visibilityFormula: visibilityFormula,
            parent: parent
        });

        this._actionButtonScript = actionButtonScript;
        this._actionButtonText = actionButtonText;
    }


    /**
     * Renders component
     * @override
     * @param {CustomActor} actor
     * @param {boolean} [isEditable=true] Is the component editable by the current user ?
     * @return {Promise<JQuery<HTMLElement>>} The jQuery element holding the component
     */
    async _getElement(actor, isEditable = true, options = {}) {
        let jQElement = await super._getElement(actor, isEditable, options);

        let buttonElement = $('<button/>');
        buttonElement.attr('type', 'text');
        buttonElement.attr('id', this.key);
        buttonElement.addClass('actionbutton');

        if (!actor.isTemplate) {
            buttonElement.attr('name', 'system.props.' + this.key);
        }

        if (!isEditable) {
            buttonElement.attr('disabled', 'disabled');
        }

        if (actor.isTemplate) {
            jQElement.addClass('custom-system-editable-component');
            buttonElement.addClass('custom-system-editable-field');
            buttonElement.html('Action: ' + (foundry.utils.getProperty(actor.system.props, this.key) ?? this._actionButtonText ?? ''));

            jQElement.on('click', (ev) => {
                ev.preventDefault();
                ev.stopPropagation();

                this.editComponent(actor);
            });
        } else {
            buttonElement.html(foundry.utils.getProperty(actor.system.props, this.key) ?? this._actionButtonText ?? '');
            buttonElement.on('click', (ev) =>{
                const thisForm = $(ev.target).parents().closest('form');

                let buttonScript = 'const thisForm = document.querySelector("form.'+thisForm[0].className.replaceAll(' ', '.')+'");';
                buttonScript += 'const thisButton = thisForm.querySelector("#'+ev.target.id+'");';
                buttonScript = ComputablePhrase.computeMessageStatic(buttonScript += this._actionButtonScript, actor.system.props, {
                    reference: this.key,
                    defaultValue: ''
                }).result;

                const callButtonScript = (buttonScript) => {
                    'use strict';
                    const buttonScriptFunction = Function(buttonScript);
                    buttonScriptFunction();
                };
                callButtonScript(buttonScript);
            });
        }

        jQElement.append(buttonElement);
        return jQElement;
    }

    /**
     * Returns serialized component
     * @override
     * @return {Object}
     */
    toJSON() {
        let jsonObj = super.toJSON();

        const jsonResponse = {
            ...jsonObj,
            actionButtonScript: this._actionButtonScript,
            actionButtonText: this._actionButtonText,
            type: 'ActionButton'
        };

        return jsonResponse;
    }

    /**
     * Creates ActionButton from JSON description
     * @override
     * @param {Object} json
     * @param {string} templateAddress
     * @param {Container|null} parent
     * @return {ActionButton}
     */
    static fromJSON(json, templateAddress, parent = null) {

        return new ActionButton({
            key: json.key,
            tooltip: json.tooltip,
            templateAddress: templateAddress,
            actionButtonScript: json.actionButtonScript,
            actionButtonText: json.actionButtonText,
            cssClass: json.cssClass,
            role: json.role,
            permission: json.permission,
            visibilityFormula: json.visibilityFormula,
            parent: parent
        });
    }


    /**
     * Gets pretty name for this component's type
     * @return {string} The pretty name
     * @throws {Error} If not implemented
     */
    static getPrettyName() {
        return 'Action Button';
    }    

    /**
     * Get configuration form for component creation / edition
     * @return {Promise<JQuery<HTMLElement>>} The jQuery element holding the component
     */
    static async getConfigForm(existingComponent) {
        let mainElt = $('<div></div>');

        mainElt.append(
            await renderTemplate(
                'modules/custom-system-builder-actionbuttons/templates/actionbutton.html',
                existingComponent
            )
        );

        return mainElt;
    }


    /**
     * Extracts configuration from submitted HTML form
     * @override
     * @param {JQuery<HTMLElement>} html The submitted form
     * @return {Object} The JSON representation of the component
     * @throws {Error} If configuration is not correct
     */
    static extractConfig(html) {
        let fieldData = super.extractConfig(html);

        if (!fieldData.key) {
            throw new Error('Component key is mandatory for Action Buttons');
        }

        fieldData.actionButtonScript = html.find('#actionButtonScript').val();
        fieldData.actionButtonText = html.find('#actionButtonText').val();

        return fieldData;
    }

}

/* Load Component when CSB is ready */
Hooks.once('customSystemBuilderReady', () => {
    componentFactory.addComponentType('ActionButton', ActionButton);
});
