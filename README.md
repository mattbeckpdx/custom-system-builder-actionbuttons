# ActionButtons Module
Extends [Custom System Builder](https://gitlab.com/custom-system-builder/custom-system-builder) to allow Javascript triggering from buttons on the sheet, with basic formula support.


## Installation:
From the Configuration and Setup screen, click Install Module. 

Paste the manifest url `https://gitlab.com/mattbeckpdx/custom-system-builder-actionbuttons/-/raw/main/module.json` into the field and install.


## Examples:

The embedded script can reference the containing form and the calling button by accessing the `thisForm` or `thisButton` variables. These will return their element nodes. 

```js
console.log(thisForm, 'this form');
console.log(thisButton, 'this button');
```

**You can use also use some formulas inside your script.**

```js
alert('${myfield}$');
````

```js
// Increment a counter stored in a number field with the key myNum
const myNum = document.querySelector('.character input[name="system.props.myNum"]');
myNum.value = ${myNum+1}$;
```
![Example](example.jpg)


## Disclaimer:
Use at your own risk.

This module uses a `Function()` call to exectute javascript from a field. Running any user-input javascript carries certain risks. 

This is restricted in scope, and I believe this to be as safe as I can make it while retaining the intended the functionality.

- [More info](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)